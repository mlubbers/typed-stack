module ppdp23

/*
Pieter Koopman & Mart Lubbers, pieter@cs.ru.nl, mart@cs.ru.nl
Radboud University Nijmegen, The Netherlands
*/

import StdEnv
import Data.Func
import Data.Functor
import Data.Functor.Identity
import Data.Tuple // hierin zit toString van een tuple
import Data.List
import Control.Applicative
import Control.Monad.State
import Control.Monad
from Text import class Text(concat), instance Text String

// ===== examples =====
Start =
  (map (\(s,t,e)->(s,prnt t,eval e)) (// Int results
    [("\npushI 1",pushI 1,pushI 1)
    ,("\nsumDef",sumDef,sumDef)
    ,("\nsumDef2",sumDef2,sumDef2)
    ,("\nsumDef3",sumDef3,sumDef3)
    ,("\nifApp",ifApp,ifApp)
    ,("\nifApp2",ifApp2,ifApp2)
    ,("\nifApp3",ifApp3,ifApp3)
    ,("\nifDef",ifDef,ifDef)
    ,("\nfacWhile 0",facWhile 0,facWhile 0)
    ,("\nfacWhile 4",facWhile 4,facWhile 4)
    ,("\nfacDef 0",facDef 0,facDef 0)
    ,("\nfacDef 4",facDef 4,facDef 4)
    ,("\nfacDefC 0",facDefC 0,facDefC 0)
    ,("\nfacDefC 4",facDefC 4,facDefC 4)
    ,("\naddMulDef",addMulDef,addMulDef)
    ,("\npush 7 :. push 5 :. sub",push 7 :. push 5 :. sub,push 7 :. push 5 :. sub)
    :[("\nhofstadter " +++ toString n,hofstadter n,hofstadter n) \\ n <- [0..6]]
    ++
    [("\ndeCode e42",deCode e42,deCode e42)
    ,("\ndeCode c42",deCode c42,deCode c42)
    ,("\nsumDef4",sumDef4,sumDef4)
    ,("\nanswer",answer,answer)
    ] ++
    [("\nhofstadter2 2",hofstadter2 2,hofstadter2 2)]
    ])
  ,map (\(s,t,e)->(s,prnt t,eval e)) // Bool results
    [("\nequDef",equDef,equDef)
    ,("\nevenDef 4",evenDef 4,evenDef 4)
    ,("\nevenDef 7",evenDef 7,evenDef 7)
    ,("\nevenDef2 4",evenDef2 4,evenDef2 4)
    ,("\nevenDef2 7",evenDef2 7,evenDef2 7)
    ,("\nevenDefC 4",evenDefC 4,evenDefC 4)
    ,("\nevenDefC 7",evenDefC 7,evenDefC 7)
    ]
  ,evalAndPrint (facDef 4)
  )

// ====== DSL + Types =====

// Stack
:: Push a stack = Push a stack
:: Bot = Bot
:: In a b = In infixr 0 a b

// Functions
:: Fun11 v a b   = Fun11 (A.s:v ((Push a s)->Push b s))
:: Fun10 v a     = Fun10 (A.s:v ((Push a s)->s))
:: Fun01 v   b   = Fun01 (A.s:v (s->Push b s))
:: Fun21 v a b c = Fun21 (A.s:v ((Push a (Push b s))->Push c s))
:: Fun12 v a b c = Fun12 (A.s:v ((Push a s)->Push b (Push c s)))

call11 :: (Fun11 v a b) -> v ((Push a s)->Push b s)
call11 (Fun11 f) = f

call10 :: (Fun10 v a) -> v ((Push a s)->s)
call10 (Fun10 f) = f

call01 :: (Fun01 v b) -> v (s->Push b s)
call01 (Fun01 f) = f

call21 :: (Fun21 v a b c) -> v ((Push a (Push b s))->Push c s)
call21 (Fun21 f) = f

call12 :: (Fun12 v a b c) -> v ((Push a s)->Push b (Push c s))
call12 (Fun12 f) = f

class arith v where
  push  :: a -> v (s->Push a s) | toString a
  pop   :: v ((Push a s) -> s)
  add   :: v ((Push a (Push a s)) -> Push a s) | + a
  sub   :: v ((Push a (Push a s)) -> Push a s) | - a
  mul   :: v ((Push a (Push a s)) -> Push a s) | * a
  equ   :: v ((Push a (Push a s)) -> Push Bool s) | == a
  neg   :: v ((Push a s) -> Push a s) | ~ a
class plumbing v where
  copy0 :: v ((Push x s) -> Push x (Push x s))
  copy1 :: v ((Push x (Push y s)) -> Push y (Push x (Push y s)))
  copy2 :: v ((Push x (Push y (Push z s))) -> Push z (Push x (Push y (Push z s))))
  trim1 :: v ((Push x (Push y s)) -> Push x s)
  trim2 :: v ((Push x (Push y (Push z s))) -> Push y (Push x s))
  no_op :: v (s->s)
class control v where
  If    :: (v (s->Push Bool u)) (v (u->t)) (v (u->t)) -> v (s->t)
  cond  :: (v (s->t)) (v (s->t)) -> v ((Push Bool s)->t)
  While :: (v (s->Push Bool s)) (v (s->s)) -> v (s->s)
  (:.) infixr 1 :: (v (a->b)) (v (b->c)) -> v (a->c)

class stack v | arith, plumbing, control v
class funs v
  | funs1 v Int
  & funs1 v Bool
  & funs2 v Int Bool
class funs1 v a
  | def v (Fun21 v a a a)
  & def v (Fun12 v a a a)
  & def v (Fun11 v a a  )
  & def v (Fun01 v   a  )
  & def v (Fun10 v a    )
class funs2 v a b
  | def v (Fun21 v a a b)
  & def v (Fun21 v a b a)
  & def v (Fun21 v b a a)
  & def v (Fun21 v a b b)
  & def v (Fun21 v b a b)
  & def v (Fun21 v b b a)
  & def v (Fun12 v a a b)
  & def v (Fun12 v a b a)
  & def v (Fun12 v b a a)
  & def v (Fun12 v a b b)
  & def v (Fun12 v b a b)
  & def v (Fun12 v b b a)
  & def v (Fun11 v a b  )
  & def v (Fun11 v b a  )
class def v a :: (a -> (In a (v b))) -> v b

// ===== evaluation view ======

:: E a =: E a

deE :: (E a) -> a
deE (E a) = a

eval :: (E (Bot->a)) -> a
eval (E f) = f Bot

instance def E a where def f = let (a In b) = f a in b
/*
instance def E (E x)           where def f = let (a In b) = f a in b
instance def E (Fun01 E a)     where def f = let (a In b) = f a in b
instance def E (Fun10 E a)     where def f = let (a In b) = f a in b
instance def E (Fun11 E a b)   where def f = let (a In b) = f a in b
instance def E (Fun21 E a b c) where def f = let (a In b) = f a in b
instance def E (Fun12 E a b c) where def f = let (a In b) = f a in b
*/
instance def E (x,y) | def E x & def E y
  where def f = let (a In b) = f (fst a, snd a) in b // to prevent cycle in spine error

instance arith E where
  push a = E \s -> Push a s
  pop   = E \(Push a s) -> s
  add   = binop (+)
  sub   = binop (-)
  mul   = binop (*)
  equ   = binop (==)
  neg   = E \(Push x s) -> Push (~ x) s
instance plumbing E where
  copy0 = E \(Push x s) -> Push x (Push x s)
  copy1 = E \(Push x (Push y s)) -> Push y (Push x (Push y s))
  copy2 = E \(Push x (Push y (Push z s))) -> Push z (Push x (Push y (Push z s)))
  trim1 = E \(Push x (Push y s)) -> Push x s
  trim2 = E \(Push x (Push y (Push z s))) -> Push y (Push x s)
  no_op = E id
instance control E where
  If c t e = c :. cond t e
  cond t e = E \(Push b s) -> deE (if b t e) s
  While c b = If c (b :. While c b) no_op
  (:.) f g = E (deE g o deE f)

binop :: (a b->c) -> E ((Push b (Push a s)) -> Push c s) // first funument deepest on stack
binop f = E \(Push b (Push a s)) -> Push (f a b) s

instance ~ Bool where ~ b = not b

// ====== print view =====

:: PS = { cnt :: Int, fun :: Int, out :: [String] -> [String]}
:: PrintMonad a :== State PS a
:: Print a = P (PrintMonad ())

prnt :: (Print a) -> String
prnt (P f) = concat ["\n":(execState f {cnt=0,out=id,fun=0}).out ["\n"]]

dePrint :: (Print a) -> PrintMonad ()
dePrint (P a) = a

(>>!) infixl 1 :: (Print a) (Print b) -> Print b // replaces >>|
(>>!) (P a) (P b) = P (a >>| b)

print :: (a -> Print b) | toString a
print = P o printM

printM :: a -> PrintMonad () | toString a
printM a = modify \s->{s & out = \acc->s.out [toString a:acc]}

instance arith Print where
  push a = print "push " >>! print a
  pop   = print "pop"
  add   = print "add"
  sub   = print "sub"
  mul   = print "mul"
  equ   = print "equ"
  neg   = print "neg"
instance plumbing Print where
  copy0 = print "copy0"
  copy1 = print "copy1"
  copy2 = print "copy2"
  trim1 = print "trim1"
  trim2 = print "trim2"
  no_op = print "no_op"
instance control Print where
  If c t e = print "If (" >>! c >>! print ") (" >>! t >>! print ") (" >>! e >>! print ")"
  cond t e = print "cond (" >>! t >>! print ") (" >>! e >>! print ")"
  (:.) f g = f >>! print " :. " >>! g >>! print ""
  While c b = print "While (" >>! c >>! print ") (" >>! b >>! print ")"

nfresh :: Int -> PrintMonad Int
nfresh n = getState >>= \s->put {s & cnt = s.cnt + n} >>| pure s.cnt

instance def Print a | toPrint a where
  def f = P $
    nfresh csize >>= \v->
    let (a In b) = f (fromPrint (P (modify \s->{s & fun=v})))
	in printM "\ndef \\" >>| printAFuns v csize >>| printM " = " >>|
	dePrint (toPrint a) >>| printM "\n  In " >>| dePrint b >>| printM ""
  where
	csize = componentSize (farity f)
	farity :: (a -> (In a (Print b))) -> Proxy a
	farity _ = Proxy

:: Proxy a = Proxy
class toPrint a where
	toPrint :: a -> Print a
	fromPrint :: (Print a) -> a
	componentSize :: (Proxy a) -> Int
instance toPrint (Print a) where
	toPrint a = a >>! print ""
	fromPrint a = printAFun a
	componentSize _ = 1
instance toPrint (Fun11 Print a b  ) where
	toPrint (Fun11 a) = print "Fun11 (" >>! a >>! print ")"
	fromPrint a = Fun11 (print "call11 " >>! printAFun a)
	componentSize _ = 1
instance toPrint (Fun10 Print a    ) where
	toPrint (Fun10 a) = print "Fun10 (" >>! a >>! print ")"
	fromPrint a = Fun10 (print "call10 " >>! printAFun a)
	componentSize _ = 1
instance toPrint (Fun01 Print   b  ) where
	toPrint (Fun01 a) = print "Fun01 (" >>! a >>! print ")"
	fromPrint a = Fun01 (print "call01 " >>! printAFun a)
	componentSize _ = 1
instance toPrint (Fun21 Print a b c) where
	toPrint (Fun21 a) = print "Fun21 (" >>! a >>! print ")"
	fromPrint a = Fun21 (print "call21 " >>! printAFun a)
	componentSize _ = 1
instance toPrint (Fun12 Print a b c) where
	toPrint (Fun12 a) = print "Fun12 (" >>! a >>! print ")"
	fromPrint a = Fun12 (print "call12 " >>! printAFun a)
	componentSize _ = 1
instance toPrint (a, b) | toPrint a & toPrint b where
	toPrint (a, b) = print "\n    (" >>! toPrint a >>! print "\n    ," >>! toPrint b >>! print ")"
	fromPrint a = (fromPrint (P (dePrint a)), fromPrint (a >>! P (modify (\s->{s & fun=s.fun+1}))))
	componentSize _ = 2

printFun :: Int -> PrintMonad ()
printFun n = printM ("f" +++ toString n)

printAFun :: (Print a) -> Print b
printAFun a = a >>! P (getState >>= \s->printFun s.fun)

printAFuns :: Int Int -> PrintMonad ()
printAFuns a 1 = printFun a
printAFuns a csize = printM "(" >>| sequence (intersperse (printM ", ") [printFun (a + i) \\ i<-[0..csize-1]]) >>| printM ")"

// ====== code generation =====

deCode :: (Code v a) -> v (s->Push a s) | stack v
deCode (Code c) = c

:: Code v a = Code (A.s:v (s->Push a s)) & stack v

class lit v :: a -> v a | toString a
class IF v :: (v Bool) (v a) (v a) -> v a
class (==.) infix 4 v a :: (v a) (v a) -> v Bool

instance lit (Code v) | stack v where lit a = Code (push a)
instance IF  (Code v) | stack v where IF (Code c) (Code t) (Code e) = Code (If c t e)
instance +   (Code v a) | stack v & + a where (+) (Code x) (Code y) = Code (x :. y :. add)
instance -   (Code v a) | stack v & - a where (-) (Code x) (Code y) = Code (x :. y :. sub)
instance *   (Code v a) | stack v & * a where (*) (Code x) (Code y) = Code (x :. y :. mul)
instance ==. (Code v) a | stack v & == a where (==.) (Code x) (Code y) = Code (x :. y :. equ)

// === tests ===

e42 :: Code v Int | stack v
e42 = (lit 6 + lit 5 - lit 4) * (lit 3 * lit 2)

c42 :: Code v Int | stack v
c42 = IF (e42 ==. lit 42) (lit 7) (lit -1)

// ====== examples / tests =====

pushI :: Int -> v (s -> Push Int s) | stack v
pushI n = push n :. push n :. add

sumDef :: v (s -> Push Int s) | stack v & def v (v (s -> Push Int s))
sumDef = def \f = push 1 :. push 2 :. add In f

sumDef2 :: (v (s -> Push Int s)) | stack v  & def v (v ((Push Int s) -> Push Int s))
sumDef2 = def \inc = push 2 :. add In push 1 :. inc :. inc

sumDef3 :: v (s -> Push Int s) | stack v & def v (Fun01 v Int)
sumDef3 = def \p1 = Fun01 (push 1) In call01 p1 :. call01 p1 :. add

sumDef4 :: v (s -> Push Int s) | stack v & def v (Fun11 v Int Int, Fun01 v Int)
sumDef4 =
  def \(inc,p1) = 
    (Fun11 (push 1 :. add)
    ,Fun01 (push 1))
  In call01 p1 :. call11 inc :. call01 p1 :. call11 inc :. call11 inc :. add

answer :: v (s -> Push Int s) | stack v & def v (Fun11 v Int Int, Fun01 v Int)
answer =
  def \(inc,arg) = 
    (Fun11 (push 1 :. add)
    ,Fun01 (push 5 :. call11 inc))
  In call01 arg :. call11 inc :. call01 arg :. mul

equDef :: v (s -> Push Bool s) | stack v & def v (v ((Push Int s)->Push Bool s))
equDef = def \f = push 1 :. equ In push 7 :. f

equDef2 :: v (s -> Push Bool s) | stack v & def v (v (s->Push Int s)) & def v (v ((Push Int s)->Push Int (Push Int s)))
equDef2 =
  def \f = push 1 In
  def \g = push 7 In g :. f :. equ

equDef3 :: v (s -> Push Bool s) | stack v & def v (Fun01 v Int) & def v (Fun21 v Int Int Bool)
equDef3 =
  def \f = Fun01 (push 1) In
  def \g = Fun21 (equ) In
  call01 f :. call01 f :. call21 g

ifApp :: (v (s->Push Int s)) | stack v
ifApp = If (push True) (push 1) (push 0)

ifApp2 :: (v (s->Push Int s)) | stack v
ifApp2 = If (push 0 :. push 1 :. equ) (push 1) (push 0)

ifApp3 :: (v (s -> Push Int s)) | stack v
ifApp3 = push 0 :. If (push 1 :. equ) (push 1) (push 0)

ifDef :: (v (s->Push Int s)) | stack v & def v (v ((Push Int s)->Push Int s))
ifDef =
  def \f = If (push 1 :. equ) (push 1) (push 0)
  In  push 4 :. f

facWhile :: Int -> (v (s->Push Int s)) | stack v
facWhile n =
  push n :. push 1 :.                   // push n and result
  While (copy1 :. push 0 :. equ :. neg) // n <> 0
    (copy1 :. mul :. 					          // result := result * n
     copy1 :. push 1 :. sub :. trim2    // n = n - 1
    ) :.
  trim1

facDef :: Int -> (v (s->Push Int s)) | stack v & def v (Fun11 v Int Int)
facDef n =
  def \fac = Fun11 (If (copy0 :. push 0 :. equ) (pop :. push 1) (copy0 :. push 1 :. sub :. call11 fac :. mul))
  In push n :. call11 fac

facDefC :: Int -> (v (s->Push Int s)) | stack v & def v (Fun11 v Int Int)
facDefC n =
  def \fac = Fun11 (copy0 :. push 0 :. equ :. cond (pop :. push 1) (copy0 :. push 1 :. sub :. call11 fac :. mul))
  In push n :. call11 fac

addMulDef :: (v (s->Push Int s)) | stack v & def v (v ((Push Bool (Push Int (Push Int s)))->Push Int s))
addMulDef =
  def \f = If no_op add mul
  In push 6 :. push 6 :. push False :. f

evenDef :: Int -> (v (s->Push Bool s)) | stack v & def v (v ((Push Int s)->Push Bool s),v ((Push Int s)->Push Bool s))
evenDef n =
  def \(even,odd) =
    (If (copy0 :. push 0 :. equ) (pop :. push True)  (push 1 :. sub :. odd)
    ,If (copy0 :. push 0 :. equ) (pop :. push False) (push 1 :. sub :. even))
  In push n :. even

evenDefC :: Int -> (v (s->Push Bool s)) | stack v & def v (v ((Push Int s)->Push Bool s),v ((Push Int s)->Push Bool s))
evenDefC n =
  def \(even,odd) =
    (copy0 :. push 0 :. equ :. cond (pop :. push True)  (push 1 :. sub :. odd)
    ,copy0 :. push 0 :. equ :. cond (pop :. push False) (push 1 :. sub :. even))
  In push n :. even

evenDef2 :: Int -> (v (s->Push Bool s)) | stack v & def v (Fun11 v Int Bool,Fun11 v Int Bool) & def v (Fun11 v Int Int)
evenDef2 n =
  def \subone = Fun11 (push 1 :. sub)
  In def \(even,odd) =
    (Fun11 (If (copy0 :. push 0 :. equ) (pop :. push True)  (call11 subone :. call11 odd))
    ,Fun11 (If (copy0 :. push 0 :. equ) (pop :. push False) (call11 subone :. call11 even)))
  In push n :. call11 even

hofstadter :: Int -> (v (s->Push Int s))
  | stack v & def v (Fun11 v Int Int,Fun11 v Int Int)
hofstadter n =
 def \(male,female) =
  (Fun11 (If (copy0 :. push 0 :. equ) (pop :. push 0)
    (copy0 :. push 1 :. sub :. call11 male :.
     call11 female :. sub))
  ,Fun11 (If (copy0 :. push 0 :. equ)
    (pop :. push 1)
    (copy0 :. push 1 :. sub :. call11 female :.
     call11 male :. sub)))
 In push n :. call11 female

hofstadter2 :: Int -> (v (s->Push Int s)) | stack v
  & def v (Fun11 v Int Int, Fun11 v Int Int)
  & def v (Fun12 v Int Bool Int, Fun12 v Int Int Int)
hofstadter2 n =
	def \(eq0, sub1) =
		( Fun12 (copy0 :. push 0 :. equ)
		, Fun12 (copy0 :. push 1 :. sub))
	In
	def \(male, female) =
		( Fun11 (call12 eq0 :. cond (pop :. push 0) (call12 sub1 :. call11 male :. call11 female :. sub))
		, Fun11 (call12 eq0 :. cond (pop :. push 1) (call12 sub1 :. call11 female :. call11 male :. sub)))
	In push 6 :. call11 female

/*
hofstadter :: Int -> (v (s->Push Int s)) | stack v & def v (Fun11 v Int Int,Fun11 v Int Int)
hofstadter n =
  def \(male,female) =
    (Fun11 (copy0 :. push 0 :. equ :. cond (pop :. push 0) (copy0 :. push 1 :. sub :. call11 male :. call11 female :. sub))
    ,Fun11 (copy0 :. push 0 :. equ :. cond (pop :. push 1) (copy0 :. push 1 :. sub :. call11 female :. call11 male :. sub)))
  In push n :. call11 female
*/

evalAndPrint :: (A.v: v (Bot->a) | stack v & funs v) -> (a, String)
evalAndPrint a = (eval a, prnt a)
