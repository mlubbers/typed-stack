Clean code for the paper "Strongly-Typed Multi-View Stack-Based Computations" submitted to PPDP 2023.
Authors: Pieter Koopman and Mart Lubbers.

Instructions on how to install and use Clean are available here: https://clean-lang.org
