#! /bin/bash
apt-get update -qq
apt-get upgrade -qq
apt-get install -qq --no-install-recommends \
    curl \
    zsh \
    git \
    git-lfs \

# Install git-lfs
git lfs install

nitrile update
nitrile fetch
nitrile global install eastwood
